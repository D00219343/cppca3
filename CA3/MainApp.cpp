#include <iostream>
#include <iomanip>
#include <vector>
#include <iterator>
#include <string>
#include "Student.h"
#include <algorithm> 
#include <iostream>
#include <fstream>

using namespace std;

bool compare(string a, string b)
{

	return a < b;
}

int* split(int grades)
{
	/**
	Splits a given 32 bit integer into 4 bit ints.
	@param 32 bit int.
	@return a pointer to an array of spliced grades.
	*/
	int* ptr = NULL;

	ptr = new int[4];
	for (int i = 0; i < 4; i++)
	{
		ptr[i] = 0;
	}


	
	unsigned char a = (grades >> 24) & 255;
	unsigned char b = (grades >> 16) & 255;
	unsigned char c = (grades >> 8) & 255;
	unsigned char d = (grades) & 255;


	ptr[0] = (int)a;
	ptr[1] = (int)b;
	ptr[2] = (int)c;
	ptr[3] = (int)d;

	return ptr;
}

void writeCurrentToFile(vector<Student> &studentStore)
{
	/*
	@Author
	www.cplusplus.com/doc/tutorial/files/
	*/

	int grades;
	char ptrToCharArr[4];
	ofstream savedStudents;
	savedStudents.open("save.txt");

	for (int i = 0; i < studentStore.size(); i++)
	{
		int tally = 0;
		int* ptr = studentStore[i].getGrades();

		for (int j = 0; j < 4; j++)
		{
			ptrToCharArr[j] = *ptr;
			ptr++;
		}
		
	}

	unsigned char a = ptrToCharArr[0];
	unsigned char b = ptrToCharArr[1];
	unsigned char c = ptrToCharArr[2];
	unsigned char d = ptrToCharArr[3];


	grades = (d << 24) | (c << 16) | (b << 8) | a;
	
		for (int i = 0; i < studentStore.size(); i++)
		{
			savedStudents << studentStore[i].getStudentId() << ';' << studentStore[i].getTitle() + ';' << studentStore[i].getName() + ';' << grades << ';' << studentStore[i].getCourse() + ';' << "\n";
		}
	
	/*else
	{
		cout << "Unable to open the specified file" << endl;
	}*/
	savedStudents.close();

}

void loadPreviousStudents(vector<Student> &studentStore)
{
	
	char delimiter = ';';
	vector<string> studentData;
	
	/*
	@Author
	www.cplusplus.com/doc/tutorial/files/
	https://www.oreilly.com/library/view/c-cookbook/0596007612/ch04s24.html
	Parts have been taken from these sites to fulfil the requirements of this function
	*/
	string input;
	ifstream savingStudents("save.txt");
	if (savingStudents.is_open()) {
		while (true)
		{
			
			int x = 0;
			getline(savingStudents, input);
			/*
			Encountered Major Errors
			Consistently Throughout this whole function
			and Refused to start it over
			https://stackoverflow.com/questions/21647/reading-from-text-file-until-eof-repeats-last-line
			was the only one I had to get reference for, the others came about from debugging such as out_of_range errors
			*/
			if (savingStudents.eof()) 
			{ 
				break;
			}
			int y = input.find(delimiter);
			for(int j = 0; j <= 4; j++)
			{

				string data = input.substr(x, (y-x));
				
				studentData.push_back(data);
				x = ++y;
				y = input.find(delimiter, y);

				if (y < 0)
				{
					studentData.push_back(input.substr(x, input.length()));
				}

			}
			int stdID = stoi(studentData[0]);
			string name = studentData[2];
			string title = studentData[1];
			string course = studentData[4];
			int grades = stoi(studentData[3]);
			

			Student readStudent(stoi(studentData[0]), studentData[1], studentData[2], split(grades), studentData[4]);
			studentStore.push_back(readStudent);
			studentData.erase(studentData.begin(), studentData.end());
			
		}
	}
	else
	{
		cout << "Unable to open the specified file" << endl;
	}
	savingStudents.close();

}

Student formatInput(int n)
{
	int studentid;
	string title;
	string name;
	
	string course;

	int* a = NULL;
	
	a = new int[n];
	for (int i = 0; i < n; i++)
	{
		a[i] = 0;
	}

	cout << "StudentID: " << endl;
	cin >> studentid;
	cout << "Title: " << endl;
	cin >> title;
	cout << "Name: " << endl;
	cin.ignore(10, '\n');
	getline(cin, name);

	cout << "Grades: (Enter each on a new Line)" << endl;
	/*Dynamic Array Taken From fredosaurus.com
	  As I couldnt remember the syntax for the 
	  dynamic array link at bottom*/	
	for (int j = 0; j < n; j++)
	{
		cin >> a[j];
	}
	cout << "Course: " << endl;
	cin >> course;
	Student student(studentid, title, name, a, course);
	return student;
}

void findStudentsByCourse(vector<Student> &studentStore, string search)
{
	vector<string> alphabeticalVector;
	for (int i = 0; i < studentStore.size(); i++)
	{
		if (studentStore[i].getCourse() == search)
		{
			alphabeticalVector.push_back(studentStore[i].getName());
		}
	}
	sort(alphabeticalVector.begin(), alphabeticalVector.end(), compare);
	cout << "Found: " << endl;
	for (int j = 0; j < alphabeticalVector.size(); j++)
	{
		cout << "\t" << alphabeticalVector[j] << endl;
	}

}

void getFailingStudents(vector<Student> &studentStore, int nScores)
{
	
	for (int i = 0; i < studentStore.size(); i++)
	{
		int tally = 0;
		int* ptr = studentStore[i].getGrades();

		for (int j = 0; j < nScores; j++)
		{
			tally += *ptr;
			
			ptr++;
		}
		if ((tally / nScores) < 40)
		{
			cout << "Student: " << endl;
			cout << "\t" << studentStore[i].getName() << " Grade: " << (tally / nScores) << endl;
		}
	}
}

Student getStudentByID(int studentID, vector<Student> &studentStore)
{
	/**
	Searches the vector for all student objects, and compares its ID with the param.

	@param searchID.
	@return A student object.
	*/

	Student copy;
	for (int i = 0; i < studentStore.size(); i++)
	{
		if (studentStore[i].getStudentId() == studentID)
		{
			copy = studentStore[i];
			cout << "Retrieved: " << copy.getName() << endl;
			return copy;
		}
		
	}
	cout << "No Student Found" << endl;
	
	return copy;
}

int main()
{
	

	vector<Student> studentStore;
	loadPreviousStudents(studentStore);
	string nameW;
	int tally = 0;
	Student* pos;
	string courseSearch;
	int searchId;
	bool validInput = false;
	int choice;
	string i;
	do {
		cout << "0:\t Quit" << endl;
		cout << "1:\t Add Student" << endl;
		cout << "2:\t Show Students on Course" << endl;
		cout << "3:\t Search by ID" << endl;
		cout << "4:\t Show Failing Students" << endl;
		cout << "5:\t Manually Save Current Session" << endl;
		cout << "6:\t Load Previous Sessions" << endl;
		cin >> choice;
		while (validInput == false) {

			validInput = true;
			if (cin.fail())
			{
				validInput = false;
				cin.clear();
				cin.ignore();
				cout << "Please enter a valid number" << endl;
				
			}

		}
			switch (choice)
			{
			case 1:
				validInput = false;
				int n;
				cout << "Enter a number for the amount of grades" << endl;
				cin >> n;
				while (validInput == false) {

					validInput = true;
					if (cin.fail())
					{
						validInput = false;
						cin.clear();
						cin.ignore();
						cout << "Please enter a valid number" << endl;

					}

				}
				cout << "Please enter the students details" << endl;
				studentStore.push_back(formatInput(n));
				pos = studentStore.data() + tally;
				nameW = pos->getName();
				cout << nameW << " Added to the student rota" << endl;
				tally++;
				break;
			case 2:
				cout << "Please enter the course name " << endl;
				cin >> courseSearch;
				findStudentsByCourse(studentStore, courseSearch);
				break;
			case 3:
				//Student Search
				cout << "Please enter the students id to search for" << endl;
				cin >> searchId;
				validInput = false;
				while (validInput == false) {

					validInput = true;
					if (cin.fail())
					{
						validInput = false;
						cin.clear();
						cin.ignore();
						cout << "Please enter a valid number" << endl;
					}
				}
				getStudentByID(searchId, studentStore).getName();
				break;
			case 4:
				getFailingStudents(studentStore, n);
				break;
			case 5:
				writeCurrentToFile(studentStore);
				break;
			default:
				break;
			}

		} while (choice != 0);
		writeCurrentToFile(studentStore);
	return 0;
}




/*
DA:http://www.fredosaurus.com/notes-cpp/newdelete/50dynamalloc.html

*/