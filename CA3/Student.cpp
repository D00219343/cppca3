
#include <iostream>
#include "Student.h"
using namespace std;

Student::Student(int sd, string te, string ne, int* ss, string ce)
{
	this->StudentID = sd;
	this->title = te;
	this->name = ne;
	this->scores = ss;
	this->course = ce;

}
Student::Student(const Student &cS)
{
	this->name = cS.name;
	this->scores = cS.scores;
	this->StudentID = cS.StudentID;
	this->title = cS.title;
	this->course = cS.course;
}
Student::~Student()
{

}

int Student::getStudentId()
{
	return this->StudentID;
}

string Student::getTitle()
{
	return this->title;
}

string Student::getName()
{
	return this->name;
}

int* Student::getGrades()
{
	return this->scores;
}

string Student::getCourse()
{
	return this->course;
}

Student::Student()
{

}

//std::ostream& operator<<(std::ostream& os, Student& s)
//{
//
//	return os << "StudentID: " << s.getStudentId() << endl
//		<< "Title: " << s.getTitle() << endl
//		<< "Name: " << s.getName() << endl
//		<< "Grades: " << s.getGrades() << endl
//		<< "Course: " << s.getCourse() << endl;
//
//}