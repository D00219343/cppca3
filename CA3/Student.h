#pragma once
#include <iostream>
using namespace std;

class Student
{
public:
	Student(int StudentID, string Title, string name, int* scores, string course);
	Student(const Student &cS);
	/*friend std::ostream& operator<<(std::ostream& os, const Student &s);*/
	~Student();
	int getStudentId();
	string getTitle();
	string getName();
	int* getGrades();
	string getCourse();
	Student();

private:
	int StudentID;
	string title;
	string name;
	int* scores;
	string course;


};


